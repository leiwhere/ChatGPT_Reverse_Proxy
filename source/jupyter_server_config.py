# Traitlet configuration file for jupyter-notebook.

c.ServerProxy.servers = {
    'testserver': {
        'command': ['python3', '-m', 'http.server' , '{port}' ],
        'port': 9090,
        'timeout': 120,
        'launcher_entry': {
            'enabled': True,
            'icon_path': '/home/jovyan/.jupyter/sin.svg',
            'title': 'r-proxy',
        },
    },
}

